﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuckTheMDPolice.Models;


namespace FuckTheMDPolice.Controllers
{
    public class CustomersController : Controller
    {
        // GET: Customers
        public ActionResult Index()
        {
			var movie = new Movie { Name = "Bikini Cops 1" };
			var customers = GetCustomers();

            return View(customers);
        }

		public ActionResult Details(int id)
		{
			var customer = GetCustomers().SingleOrDefault(c => c.Id == id);

			if (customer == null)
				return HttpNotFound();

			return View(customer);
		}

		private IEnumerable<Customer> GetCustomers()
		{
			return new List<Customer>
			{
				new Customer { Id = 1, Name = "Bad Cop"},
				new Customer { Id = 2, Name = "Good Cop"},
				new Customer { Id = 3, Name = "Ugly Cop"}
			};
		}


    }
}