﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuckTheMDPolice.Models;


namespace FuckTheMDPolice.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Index()
        {
			var movies = GetMovies();

			return View(movies);
        }

		private IEnumerable<Movie> GetMovies()
		{
			return new List<Movie>
			{
				new Movie { Id = 1, Name = "Bikini Cops"},
				new Movie { Id = 2, Name = "Bikini Cops Gone Wild"},
				new Movie { Id = 3, Name = "The Bad Cop, The Good Cop and The Ugly Cop"}
			};
		}
    }
}